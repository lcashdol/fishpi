#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <locale.h>
#include <wchar.h>


void
main (int argc, char *argv[])
{
setlocale(LC_ALL, "");
int x=10,y=20;
  initscr ();
  refresh ();
while (x < 20) {
    //we should get the terminal size from the environment 
    y = abs(y) % 80;
    x = abs(x) % 24;

      mvprintw (x,x,"         /////\n");
      mvprintw (x+1,x,"  /o))#######---/\n");
      mvprintw (x+2,x," =))#########---B\n");
      mvprintw (x+3,x,"    \\ V  ###----\\\n");
      mvprintw (x+2,y+20,"         /////\n");
      mvprintw (x+3,y+20,"  /o))#######---/\n");
      mvprintw (x+4,y+20," =))#########---B\n");
      mvprintw (x+5,y+20,"    \\ V  ###----\\\n");
mvprintw (y,y+5,"   .----.\n");
mvprintw (y+1,y+5,"____    __\\\\\\__  \n");
mvprintw (y+2,y+5,"\\___'--\"          .-.\n");
mvprintw (y+3,y+5," /___<             '0'\n");
mvprintw (y+4,y+5,"/____,--.       y     B\n");
mvprintw (y+5,y+5,"        \"\".____  ___-\" \n");
mvprintw (y+6,y+5,"          //    / /   \n");
mvprintw (y+7,y+5,"                ]/        \n");
mvprintw(0,0,"[%d] [%d]",x,y);

refresh();
sleep(1);
clear();
if ((rand()%5) == 2) y--;
if ((rand()%5) == 2) y++;
if ((rand()%5) == 2) x--;
if ((rand()%5) == 2) x++;
}

system("reset");
}

void 
initcon (void)
{
  initscr ();
  noecho ();
  cbreak ();
//  refresh ();
}